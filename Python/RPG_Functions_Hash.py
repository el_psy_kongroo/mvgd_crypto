import pickle
import hashlib
import hmac

from RPG_Functions import CreateAppleGel
from Heal_Item import Heal_Item
from Character import Character
 

def WriteCharacterHash(hmac_key):
	c = Character("Kratos")
	c.hitpoints = 250
	c.inventory.append(CreateAppleGel())
	char_bytes = pickle.dumps(c)

	hmac_obj = hmac.new(hmac_key, char_bytes, hashlib.sha256)
	hmac_bytes = hmac_obj.digest()

	char_file = open("charsave_hash.dat", "wb")
	sig_file = open("charsave_hash.sig", "wb")

	char_file.write(char_bytes)
	char_file.close()

	sig_file.write(hmac_bytes)
	sig_file.close()


def ReadCharacterHash(hmac_key, c):
	char_file = open("charsave_hash.dat", "rb")
	sig_file = open("charsave_hash.sig", "rb")

	char_bytes = char_file.read()
	sig_bytes = sig_file.read()

	char_file.close()
	sig_file.close()

	hmac_obj = hmac.new(hmac_key, char_bytes, hashlib.sha256)
	char_file_hash = hmac_obj.digest()

	if hmac.compare_digest(char_file_hash, sig_bytes):
		c = pickle.loads(char_bytes)
		return True

	return False
