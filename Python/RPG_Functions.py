import pickle


from Heal_Item import Heal_Item
from Character import Character



def CreateAppleGel():
    apple_gel = Heal_Item("Apple Gel", "Medicinal gel. Heals one ally's HP by 30%. "
                                        "Cannot be used while afflicted by a status ailment.", .3)
    return apple_gel

def PrintCharacterInfo(c):
	print("Character's name: " + c.name)
	print("HP: " + str(c.hitpoints))
	print("MP: " + str(c.mana))

	inv_size = len(c.inventory)
	
	print("Inventory count: " + str(inv_size))

	if inv_size > 0:
		print("")
		print("Inventory contents")
		print("==================")
		for item in c.inventory:
			print("Name: " + item.name)
			print("Description: " + item.description)


def WriteCharacter():
	c = Character("Jude")
	c.inventory.append(CreateAppleGel())
	char_file = open("charsave.dat", "wb")
	pickle.dump(c, char_file)
	char_file.close()

def ReadCharacter():
	char_file = open("charsave.dat", "rb")
	c = pickle.load(char_file)
	char_file.close()
	return c
