#!/usr/bin/python

import pickle
import sys

from RPG_Functions import *
from RPG_Functions_Hash import *


from Character import Character
from Inventory_Item import Inventory_Item
from Heal_Item import Heal_Item


hmac_key = bytearray([0x49, 0x2A, 0x29, 0x39, 0x9D, 0x29, 0xB4, 0x53,
                 0xF1, 0xC2, 0xB7, 0x79, 0xC5, 0x22, 0x40, 0x3C,
                 0x99, 0x75, 0x59, 0x2C, 0xED, 0xE7, 0x0F, 0x5B,
                 0x41, 0xA6, 0x6F, 0x59, 0x15, 0xDD, 0x84, 0x38])


if len(sys.argv) < 2:
	print(sys.argv[0] + ": Missing command.")
	sys.exit(1)


cmd = sys.argv[1]


if cmd == "read":
	print("Attempting to read character from disk...")
	char = ReadCharacter()
	PrintCharacterInfo(char)

elif cmd == "write":
	WriteCharacter()
	print("Created and wrote character to disk")

elif cmd == "readhash":
	char = Character("")

	if ReadCharacterHash(hmac_key, char):
		print("Verified signature on character file and loaded succesfully")
		PrintCharacterInfo(char)
	else:
		print("Character file failed integrity check!")


elif cmd == "writehash":
	WriteCharacterHash(hmac_key)

else:
	print(sys.argv[0] + ": Invalid command.")
	sys.exit(1)


