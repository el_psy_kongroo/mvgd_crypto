This project contains all the code examples from the March 11, 2020
MVGD meetup talk.

If you are trying to compile the C++ examples, there are CMAKE files
to aid in that process. These should work in Unix and macos. I have
not attempted to compile in Windows.
