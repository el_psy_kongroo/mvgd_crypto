#pragma once

#include <cereal/cereal.hpp>

#include "inventory_item.h"

class Heal_Item : public virtual Inventory_Item
{
	public:
		
		Heal_Item(std::string const &name, std::string const &description, float percent);
		~Heal_Item();


		float heal_percent;

        #pragma GCC diagnostic push
        #pragma GCC diagnostic ignored "-Wunused-parameter"

	    template<class Archive>
        void load(Archive &archive, uint32_t const version)
        {
            archive(cereal::virtual_base_class<Inventory_Item>(this), heal_percent);
        }

        template<class Archive>
        void save(Archive &archive, uint32_t const version) const
        {
            archive(cereal::virtual_base_class<Inventory_Item>(this), heal_percent);
        }

		#pragma GCC diagnostic pop

};

CEREAL_CLASS_VERSION(Heal_Item, 1)

namespace cereal
{
    template <class Archive>
    struct specialize<Archive, Heal_Item, cereal::specialization::member_load_save> {};
}

