#pragma once

#include <string>

#include <cereal/cereal.hpp>
#include <cereal/types/string.hpp>
#include <cereal/archives/portable_binary.hpp>
#include <cereal/types/base_class.hpp>


class Inventory_Item
{
	public:

		Inventory_Item();
		Inventory_Item(std::string const &name, std::string const &description);
		~Inventory_Item();

		std::string item_name;
		std::string item_description;


        #pragma GCC diagnostic push
        #pragma GCC diagnostic ignored "-Wunused-parameter"

        template<class Archive>
        void load(Archive &archive, uint32_t const version)
        {
            archive(item_name, item_description);
        }

        template<class Archive>
        void save(Archive &archive, uint32_t const version) const
        {
            archive(item_name, item_description);
        }

		#pragma GCC diagnostic pop

};

CEREAL_CLASS_VERSION(Inventory_Item, 1)
