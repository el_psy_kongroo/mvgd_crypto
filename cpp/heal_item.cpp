#include "heal_item.h"

Heal_Item::Heal_Item(std::string const &name, std::string const &description,
					 float percent) : Inventory_Item(name, description)
{
	this->heal_percent = percent;
}

Heal_Item::~Heal_Item()
{

}
