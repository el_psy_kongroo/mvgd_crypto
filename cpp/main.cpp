#include <iostream>
#include <fstream>
#include <map>
#include <string>
#include <sstream>


#include <cereal/cereal.hpp>
#include <cereal/archives/binary.hpp>
#include <cereal/archives/portable_binary.hpp>

#include <cryptopp/files.h>
#include <cryptopp/cryptlib.h>
#include <cryptopp/filters.h>
#include <cryptopp/sha.h>
#include <cryptopp/hmac.h>
#include <cryptopp/modes.h>

#include "character.h"
#include "heal_item.h"

enum class CmdType
{
	READ,
	WRITE,
	READ_HASH,
	WRITE_HASH
};


std::map<std::string, CmdType> const cmdMap =
{
    {"read", CmdType::READ},
    {"write", CmdType::WRITE},
    {"readhash", CmdType::READ_HASH},
    {"writehash", CmdType::WRITE_HASH}
};

static uint8_t hmac_key[] = {0x4F, 0xAF, 0xD4, 0x10, 0x7B, 0xE9, 0xB7, 0x89,
							 0x52, 0x17, 0x99, 0x0F, 0xF6, 0xB6, 0xE8, 0x77,
							 0x78, 0xC7, 0xDC, 0x5B, 0x8A, 0x4D, 0xC1, 0xF7,
							 0x3B, 0xF8, 0x4B, 0x4B, 0xFB, 0x43, 0xAF, 0x93};

std::shared_ptr<Heal_Item> CreateAppleGel();

void PrintCharacterInfo(Character const &chr);

bool WriteCharacterHash();
bool ReadCharacterHash();

int main(int argc, char* argv[])
{
	// Ensure sufficient arg count
	if (argc < 2)
	{
		std::cout << argv[0] << ": No command provided." << std::endl;
		return 1;
	}

	// Lookup command in map
    std::string cmdStr = argv[1];

	// Did not find command
    if (cmdMap.find(cmdStr) == cmdMap.cend())
	{
		std::cout << argv[0] << ": Invalid command." << std::endl;
        return 1;
	}

	// Command exists, get its enum
    CmdType cmd = cmdMap.at(cmdStr);

	switch (cmd)
	{
		case CmdType::READ:
		{
			// Attempt to open saved character file and deserialize it

			std::ifstream in_file("charsave.dat", std::ios::binary);
			
			if (!in_file)
			{
				std::cout << "Unable to open character file" << std::endl;
				return 1;
			}
			
			std::cout << "Attempting to display character information..." << std::endl;

			cereal::PortableBinaryInputArchive ia(in_file);

			Character chr;

			// Deserialize
			ia(chr);
			in_file.close();

			PrintCharacterInfo(chr);

			break;
		}

        case CmdType::WRITE:
        {
			// Create a character and save it to disk
			Character jude("Jude");

			std::shared_ptr<Heal_Item> apple_gel = CreateAppleGel();
			jude.inventory.push_back(apple_gel);

			std::ofstream out_file("charsave.dat", std::ios::binary);

			if (!out_file)
			{
				std::cout << "Unable to save character to disk." << std::endl;
				return 1;
			}

			cereal::PortableBinaryOutputArchive oa(out_file);
			oa(jude);
			
			out_file.close();

            break;
        }

        case CmdType::READ_HASH:
        {
			if (!ReadCharacterHash())
				return 1;

            break;
        }

        case CmdType::WRITE_HASH:
        {
			if (!WriteCharacterHash())
				return 1;

            break;
        }
	}

	return 0;
}

void PrintCharacterInfo(Character const &chr)
{
	// Prints the supplied character's stats and inventory contents

	std::cout << "Character's name: " << chr.name << std::endl;
	std::cout << "HP: " << chr.hitpoints << std::endl;
	std::cout << "TP: " << chr.mana << std::endl;
	
	std::cout << "Inventory count: " << chr.inventory.size() << std::endl;
	std::cout << std::endl;

	std::cout << "Inventory contents" << std::endl;
	std::cout << "==================" << std::endl;

    for (std::shared_ptr<Inventory_Item> const item : chr.inventory)
	{
        std::cout << "Name: " << item->item_name << std::endl;
		std::cout << "Description: " << item->item_description << std::endl;
	}
}

std::shared_ptr<Heal_Item> CreateAppleGel()
{
	// Creates a new Apple Gel Heal_Item on the heap and returns the shared_ptr to it
	return std::make_shared<Heal_Item>("Apple Gel",
										"Medicinal gel. Heals one ally's HP by 30%. "\
										"Cannot be used while afflicted by a status ailment.", .3f);
}

bool WriteCharacterHash()
{
	// For the HMAC key
	CryptoPP::SecByteBlock hmacSBB(32);

	// Copy key to SecByteBlock
	for (size_t i = 0; i < hmacSBB.size(); i++)
		hmacSBB[i] = hmac_key[i];


	// Create character
    Character kratos("Kratos");
	kratos.hitpoints = 220;

    std::shared_ptr<Heal_Item> apple_gel = CreateAppleGel();
    kratos.inventory.push_back(apple_gel);

	// Memory stream to hold serialized character data
	std::stringstream serialized(std::ios::binary | std::ios::in | std::ios::out);

	// Serialize
    cereal::PortableBinaryOutputArchive oa(serialized);
    oa(kratos);

	// Get total size of serialized data
	serialized.seekg(0, std::ios::end);
	size_t serialized_size = serialized.tellg();

	// Move back to start of memory stream and clear eof bit
	serialized.seekg(0, std::ios::beg);
	serialized.clear();

	// The hmac object which will be used to generate the hash signature
	// SHA256 is used here
	CryptoPP::HMAC<CryptoPP::SHA256> hmac(hmacSBB, hmacSBB.size());

	std::ofstream hmac_outfile("charsave_hash.sig", std::ios::binary);

	if (!hmac_outfile)
	{
		std::cout << "WriteCharacterHash: Unable to write character hmac signature to disk" << std::endl;
		return false;
	}

	// Obtain the hmac hash on the serialized data and write it to disk
	CryptoPP::FileSource(serialized, true, new CryptoPP::HashFilter(hmac,
						 new CryptoPP::FileSink(hmac_outfile)));

	hmac_outfile.close();

	// Reset memory stream to beginning
	serialized.seekg(0, std::ios::beg);
	serialized.clear();

	std::ofstream out_file("charsave_hash.dat", std::ios::binary);

	if (!out_file)
	{
		std::cout << "WriteCharacterHash: Unable to write character data to disk" << std::endl;
		return false;
	}

	// Dump memory stream to disk
	out_file.write(serialized.str().c_str(), serialized_size);
	out_file.close();	
	
	return true;
}

bool ReadCharacterHash()
{
    CryptoPP::SecByteBlock hmacSBB(32);

    for (size_t i = 0; i < hmacSBB.size(); i++)
        hmacSBB[i] = hmac_key[i];


	std::stringstream data_ss(std::ios::binary | std::ios::in | std::ios::out);

	if (!data_ss)
	{
		std::cout << "Failed to allocate data_ss buffer" << std::endl;
		return false;
	}

	std::ifstream in_file_data("charsave_hash.dat", std::ios::binary);

	if (!in_file_data)
	{
		std::cout << "Failed opening charsave_hash.dat" << std::endl;
		return false;
	}

	std::ifstream in_file_sig("charsave_hash.sig", std::ios::binary);

	if (!in_file_sig)
	{
		std::cout << "Failed opening charsave_hash.sig" << std::endl;
		return false;
	}

	// Copy signature and data into data_ss

	data_ss << in_file_sig.rdbuf();
	data_ss << in_file_data.rdbuf();

	// data_ss now has memory layout of [ hash signature | data ]

	size_t sig_size = in_file_sig.tellg();

	in_file_data.close();
	in_file_sig.close();

	data_ss.clear();

	data_ss.seekg(0, std::ios::beg);


	// Tell CryptoPP to expect data layout of [ hash signature | data ],
	// and put the verification result into supplied location

	const static int cryptopp_flags = CryptoPP::HashVerificationFilter::PUT_RESULT |
                CryptoPP::HashVerificationFilter::HASH_AT_BEGIN;



	bool hmac_verified = false;

	CryptoPP::HMAC<CryptoPP::SHA256> hmac(hmacSBB, hmacSBB.size());

	CryptoPP::FileSource(data_ss, true, new CryptoPP::HashVerificationFilter(hmac,
							new CryptoPP::ArraySink((uint8_t *)&hmac_verified,
								sizeof(hmac_verified)), cryptopp_flags));

	
	if (!hmac_verified)
	{
		std::cout << "Character save data failed hash integrity check!" << std::endl;
		return false;
	}

	data_ss.clear();
	data_ss.seekg(sig_size, std::ios::beg);

	cereal::PortableBinaryInputArchive ia(data_ss);

	Character chr;

	ia(chr);

	PrintCharacterInfo(chr);	

	return true;
}
