#pragma once

#include <vector>
#include <string>
#include <memory>
#include <cereal/cereal.hpp>
#include <cereal/types/string.hpp>
#include <cereal/types/vector.hpp>
#include <cereal/archives/portable_binary.hpp>
#include <cereal/types/memory.hpp>

#include "inventory_item.h"

class Character
{
	public:
		
		Character();
		Character(std::string const &name);
		~Character();

        uint32_t MaxHP();
        uint32_t MaxMana();

		std::string name;
		uint32_t hitpoints;
		uint32_t mana;	
		std::vector<std::shared_ptr<Inventory_Item>> inventory;

        #pragma GCC diagnostic push
        #pragma GCC diagnostic ignored "-Wunused-parameter"

        template<class Archive>
        void load(Archive &archive, uint32_t const version)
        {
			archive(name, hitpoints, mana, inventory);
		}

		template<class Archive>
        void save(Archive &archive, uint32_t const version) const
        {
			archive(name, hitpoints, mana, inventory);
		}

		#pragma GCC diagnostic pop

};

CEREAL_CLASS_VERSION(Character, 1)
